import './App.css';
import React, { Component } from 'react';
import axios from 'axios';
import AllProducts from './components/AllProducts';
import Banner from './components/Banner';
import Loader from './components/Loader';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from './components/Footer';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: '',
      isLoaded: false,
      errorInData: false,
      idReturnedByFakeAPI: '',
    };
  }
  componentDidMount() {
    axios
      .get('https://fakestoreapi.com/products')
      .then((res) =>
        this.setState({ products: res.data }, () => {
          this.setState({ isLoaded: true });
        })
      )
      .then(() => console.log(this.state.products))
      .catch(() => this.setState({ errorInData: true }));
  }

  addNewProduct = (newProduct, returnedId) => {
    const copyProducts = [...this.state.products];
    newProduct[0]['id'] = returnedId.data.id;
    newProduct[0].rating = {
      rate: 4.6,
      count: 145,
    };
    copyProducts.push(newProduct[0]);
    this.setState({ idReturnedByFakeAPI: returnedId.data.id }, () => {
      this.setState({ products: copyProducts });
    });
  };

  updateProduct = (productData, returnedId) => {
    console.log(productData);
    const copyProducts = [...this.state.products];
    const indexOfObject = copyProducts.findIndex((elem) => elem.id == returnedId.data.id);
    console.log(indexOfObject, copyProducts, returnedId);
    copyProducts[indexOfObject].title = productData[0].title;
    copyProducts[indexOfObject].description = productData[0].description;
    copyProducts[indexOfObject].price = productData[0].price;
    copyProducts[indexOfObject].image = productData[0].image;
    copyProducts[indexOfObject].category = productData[0].category;
    this.setState({ products: copyProducts }, () => {
      console.log('Products updated');
    });
  };

  render() {
    if (this.state.products === '') {
      return (
        <div>
          <Banner />
          {this.state.isLoaded ? <h1>Data empty/not received</h1> : <Loader />}
          <Footer />
        </div>
      );
    } else if (this.state.errorInData) {
      return (
        <div>
          <Banner />
          <h1>Error in receiving data</h1>
          <Footer />
        </div>
      );
    } else {
      return (
        <div>
          <Banner />
          {this.state.isLoaded ? (
            <AllProducts
              products={this.state.products}
              returnedId={this.state.idReturnedByFakeAPI}
              addNewProduct={this.addNewProduct}
              updateProduct={this.updateProduct}
            />
          ) : (
            <Loader />
          )}
          <Footer />
        </div>
      );
    }
  }
}

export default App;
