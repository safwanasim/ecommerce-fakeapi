import React, { Component } from 'react';
import ModalForm from './ModalForm';
import UpdateModalForm from './UpdateModalForm';
import axios from 'axios';

const plus = require('../images/22623236.jpg');

export default class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isOpenUpdate: false,
    };
  }
  openModal = () => this.setState({ isOpen: true });
  closeModal = () => this.setState({ isOpen: false });

  //updatingModal
  openUpdateModal = () => this.setState({ isOpenUpdate: true });
  closeUpdateModal = () => this.setState({ isOpenUpdate: false });

  //fetching data from FAKEAPI for post request
  handleSubmit = async (...newProductData) => {
    let response = await axios.post(
      'https://fakestoreapi.com/products',
      JSON.stringify(newProductData)
    );
    console.log(response);
    this.closeModal();
    this.props.addNewProduct(newProductData, response);
  };

  handleUpdateSubmit = async (...productsData) => {
    let response = await axios.patch(
      `https://fakestoreapi.com/products/${productsData[0].id}`,
      JSON.stringify(productsData)
    );
    console.log(response);
    this.closeUpdateModal();
    this.props.updateProduct(productsData, response);
  };

  render() {
    return (
      <div className="col-lg-4 col-md-6 col-sm-6 my-4">
        <div className="card mx-2 h-100">
          <img
            className="card-img-top w-75 my-4 mr-auto ml-auto"
            style={{ height: '250px' }}
            src={plus}
            alt="Card cap"
          />
          <div className="card-body bg-secondary text-light text-center">
            <h5 className="card-title">Manage Products</h5>
            <hr className="bg-white" />
            <div className="text-center">
              <button
                type="button"
                className="btn btn-outline-light mt-4 mx-auto"
                data-mdb-ripple-color="dark"
                onClick={this.openModal}
              >
                Click to add Product
              </button>
              <button
                type="button"
                className="btn btn-outline-light mt-4 mx-auto"
                data-mdb-ripple-color="dark"
                onClick={this.openUpdateModal}
              >
                Click to update Product
              </button>
            </div>
          </div>
          <div className="card-footer bg-secondary text-light"></div>
        </div>
        {this.state.isOpen ? (
          <ModalForm
            closeModal={this.closeModal}
            isOpen={this.state.isOpen}
            handleSubmit={this.handleSubmit}
          />
        ) : null}
        {this.state.isOpenUpdate ? (
          <UpdateModalForm
            closeModal={this.closeUpdateModal}
            isOpen={this.state.isOpenUpdate}
            handleSubmit={this.handleUpdateSubmit}
            allProducts={this.props.allProducts}
          />
        ) : null}
      </div>
    );
  }
}
