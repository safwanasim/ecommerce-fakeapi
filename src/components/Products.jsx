import React from 'react';
import './styles/products.css';

const pStyle = `.card-text {
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
}`;

export default function Products(props) {
  console.log(props.image);
  return (
    <div className="col-lg-4 col-md-6 col-sm-6 my-4">
      <div className="card mx-2 h-100">
        <style>{pStyle}</style>
        <img
          className="card-img-top w-75 my-4 mr-auto ml-auto"
          style={{ height: '250px' }}
          src={props.image}
          alt="Card cap"
        />
        <div className="card-body bg-black text-light">
          <h5 className="card-title">{props.title}</h5>
          <hr className="bg-white" />
          <p className="card-text">{props.description}</p>
        </div>
        <div className="card-footer bg-black text-light">
          <small className="text-white h5-responsive">{'₹' + props.price * 100}</small>
          <small className="text-white h5-responsive float-right">
            Rating: {props.rating} ({props.count})
          </small>
        </div>
      </div>
    </div>
  );
}
