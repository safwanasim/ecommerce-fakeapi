import React from 'react';
import './styles/loader.css';

export default function Loader() {
  return (
    <div>
      <div className="loader google-loader">
        <div className="google-loader-bar"></div>
        <div className="google-loader-bar"></div>
        <div className="google-loader-bar"></div>
        <div className="google-loader-bar"></div>
      </div>
    </div>
  );
}
