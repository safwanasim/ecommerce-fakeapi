import React from 'react';

export default function Banner() {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-light bg-white">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-mdb-toggle="collapse"
            data-mdb-target="#navbarExample01"
            aria-controls="navbarExample01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="fas fa-bars"></i>
          </button>
          <div className="collapse navbar-collapse" id="navbarExample01">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item active">
                <a className="nav-link" aria-current="page" href="#">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Features
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Pricing
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  About
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div
        className="p-5 text-center img-fluid bg-image shadow-1-strong"
        style={{
          backgroundImage: `url(${`https://mdbcdn.b-cdn.net/img/new/slides/041.webp`})`,
          height: '45vh',
          backgroundSize: '100% auto',
          backgroundRepeat: 'no-repeat',
          // opacity: 0.5,
          //   objectFit: 'cover',
        }}
      >
        <div className="mask" style={{ backgroundColor: 'rgba(0, 0, 0, 0.4)' }}>
          <div className="d-flex justify-content-center align-items-center my-3 py-5 h-100">
            <div className="text-white">
              <h1 className="mb-3">Winter Sale</h1>
              <h4 className="mb-3">Discounts upto 60%!</h4>
              <a className="btn btn-outline-white btn-lg text-white" href="#!" role="button">
                Go to Promotional Offers
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}
