import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

export default class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      price: '',
      image: '',
      category: '',
    };
  }
  handleChange = (e) =>
    this.setState({ ...this.state, [e.target.name]: e.target.value }, () => {
      console.log(this.state);
    });
  render() {
    return (
      <>
        <div className="d-flex align-items-center justify-content-center"></div>
        <Modal show={this.props.isOpen} onHide={this.props.closeModal} backdrop="static">
          <Modal.Header closeButton>
            <Modal.Title>Enter Product Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group>
              <Form.Label className="mt-1 mb-0">Title: </Form.Label>
              <Form.Control
                type="text"
                name="title"
                onChange={this.handleChange}
                value={this.state.title}
                placeholder="title"
              />
              <Form.Label className="mt-1 mb-0">Description: </Form.Label>
              <Form.Control
                type="text"
                name="description"
                onChange={this.handleChange}
                value={this.state.description}
                placeholder="Description"
              />
              <Form.Label className="mt-1 mb-0">Price: </Form.Label>
              <Form.Control
                type="number"
                name="price"
                onChange={this.handleChange}
                value={this.state.price}
                placeholder="Price"
              />
              <Form.Label className="mt-1 mb-0">Img Source: </Form.Label>
              <Form.Control
                type="text"
                name="image"
                onChange={this.handleChange}
                value={this.state.image}
                placeholder="Enter Link to image"
              />
              <Form.Label className="mt-1 mb-0">Category: </Form.Label>
              <Form.Control
                type="text"
                name="category"
                onChange={this.handleChange}
                value={this.state.category}
                placeholder="category"
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="primary"
              type="submit"
              onClick={() => this.props.handleSubmit(this.state)}
            >
              Submit
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}
