import React from 'react';
import Products from './Products';
import AddProduct from './AddProduct';

export default function AllProducts(props) {
  return (
    <div className="container-xxl d-flex my-5 flex-wrap justify-content-start">
      {props.products.map((item) => (
        <Products
          key={item.id}
          image={item.image}
          title={item.title}
          description={item.description}
          price={item.price}
          rating={item.rating.rate}
          count={item.rating.count}
        />
      ))}
      <AddProduct
        key={props.returnedId}
        addNewProduct={props.addNewProduct}
        updateProduct={props.updateProduct}
        allProducts={props.products}
      />
    </div>
  );
}
